<?php 
//ini_set('display_errors', '1');
//ini_set('error_reporting',E_ALL);
include("conexion/conec.php");
session_start();
include("conexion/globales/globals.php");
if($_SESSION["id"]>0){$login=1;}
?>
<!doctype html>
<html class="no-js h-100" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>..:H2OBRAS:..</title>
    <meta name="description" content="H2OBRAS COLECTOR COTIZADOR">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" href="images/favico.png" sizes="64x64" />
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" id="main-stylesheet" data-version="1.0.0" href="styles/shards-dashboards.1.0.0.min.css">
    <link rel="stylesheet" href="styles/extras.1.0.0.min.css">
    <script async defer src="https://buttons.github.io/buttons.js"></script>
  </head>
  <body class="h-100" onload="scrollWin()";>
    
    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
        <aside class="main-sidebar col-12 col-md-3 col-lg-2 px-0">
          <div class="main-navbar">
            <nav class="navbar align-items-stretch navbar-light bg-white flex-md-nowrap border-bottom p-0">
              <a class="navbar-brand w-100 mr-0" href="#" style="line-height: 25px;">
                <div class="d-table m-auto">
                  <img id="main-logo" class="d-inline-block align-top mr-1" style="max-width: 150px;" src="images/h2obras.jpg" alt="">
                  <span class="d-none d-md-inline ml-1"></span>
                </div>
              </a>
              <a class="toggle-sidebar d-sm-inline d-md-none d-lg-none">
                <i class="material-icons">&#xE5C4;</i>
              </a>
            </nav>
          </div>
          <?php /*
          <form action="#" class="main-sidebar__search w-100 border-right d-sm-flex d-md-none d-lg-none">
            <div class="input-group input-group-seamless ml-3">
              <div class="input-group-prepend">
                <div class="input-group-text">
                  <i class="fas fa-search"></i>
                </div>
              </div>
              <input class="navbar-search form-control" type="text" placeholder="Search for something..." aria-label="Search"> </div>
          </form>*/?>
          <div class="nav-wrapper">
          <?php if($login){ ?>
            <ul class="nav flex-column">
              <li class="nav-item">
                <a <?php if(empty($_GET["page"])){?> class="nav-link active" <?php }else{ ?>class="nav-link " <?php } ?> href="index.php">
                  <i class="material-icons">edit</i>
                  <span>Inicio</span>
                </a>
              </li>
              <?php if($_SESSION["tipo"]=="admin"){ ?>
              <li class="nav-item">
                <a <?php if($_GET["page"]=="cat"){?>class="nav-link active"<?php }else{ ?>class="nav-link "<?php }?> href="index.php?page=cat">
                  <i class="material-icons">vertical_split</i>
                  <span>Catalogo</span>
                </a>
              </li>
              <?php } ?>
              <?php if($_SESSION["tipo"]=="admin"){ ?>
              <li class="nav-item">
                <a <?php if($_GET["page"]=="cli"){?>class="nav-link active"<?php }else{ ?>class="nav-link "<?php }?> href="index.php?page=cli">
                  <i class="material-icons">note_add</i>
                  <span>Clientes</span>
                </a>
              </li>
              <?php } ?>
              <li class="nav-item">
                <a <?php if($_GET["page"]=="ven"){?>class="nav-link active"<?php }else{ ?>class="nav-link "<?php }?> href="index.php?page=ven">
                  <i class="material-icons">view_module</i>
                  <span>Cotizar</span>
                </a>
              </li>
              <li class="nav-item">
                <a <?php if($_GET["page"]=="usr"){?>class="nav-link active"<?php }else{ ?>class="nav-link "<?php }?> href="index.php?page=usr">
                  <i class="material-icons">person</i>
                  <span>Datos de Usuario</span>
                </a>
              </li>
             </ul>
             <?php } ?>
          </div>
        </aside>
        <!-- End Main Sidebar -->
        <main class="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
          <div class="main-navbar sticky-top bg-white">
            <!-- Main Navbar -->
            <?php if($login){?>
            <nav class="navbar align-items-stretch navbar-light flex-md-nowrap p-0">
              <form action="#" class="main-navbar__search w-100 d-none d-md-flex d-lg-flex">
                <div class="input-group input-group-seamless ml-3">
                  <div class="input-group-prepend">
                    
                  </div>
                  <input class="navbar-search form-control" type="text" placeholder="H2OBRAS SRL" aria-label="Search"> </div>
              </form>
              <ul class="navbar-nav border-left flex-row ">
                <li class="nav-item border-right dropdown notifications">
                  <a class="nav-link nav-link-icon text-center" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="nav-link-icon__wrapper">
                      <i class="material-icons">&#xE7F4;</i>
                      <span class="badge badge-pill badge-danger">2</span>
                    </div>
                  </a>
                  <div class="dropdown-menu dropdown-menu-small" aria-labelledby="dropdownMenuLink">
                    <a class="dropdown-item" href="#">
                      <div class="notification__icon-wrapper">
                        <div class="notification__icon">
                          <i class="material-icons">&#xE6E1;</i>
                        </div>
                      </div>
                      <div class="notification__content">
                        <span class="notification__category">Solicitudes</span>
                        <p>Tiene Ordenes de Compra pendientes por despachar. Total
                          <span class="text-success text-semibold">10</span> ordenes de colectores</p>
                      </div>
                    </a>
                    <a class="dropdown-item" href="#">
                      <div class="notification__icon-wrapper">
                        <div class="notification__icon">
                          <i class="material-icons">&#xE8D1;</i>
                        </div>
                      </div>
                      <div class="notification__content">
                        <span class="notification__category">Ventas</span>
                        <p>Se han despachado 
                          <span class="text-danger text-semibold">32</span> colectores en el mes actual</p>
                      </div>
                    </a>
                    <a class="dropdown-item notification__all text-center" href="#"> Ver todas las notificaciones </a>
                  </div>
                </li>
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle text-nowrap px-3" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <img class="user-avatar rounded-circle mr-2" src="images/avatars/4.jpg" alt="User Avatar">
                    <span class="d-none d-md-inline-block"><?php echo $nombre_usuario;?></span>
                  </a>
                  <div class="dropdown-menu dropdown-menu-small">
                    <a class="dropdown-item" href="index.php?page=usr">
                      <i class="material-icons">&#xE7FD;</i> Datos</a>
                    <a class="dropdown-item" href="#passModal" data-toggle="modal" data-target="#passModal">
                      <i class="material-icons">vertical_split</i> Cambiar Password</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item text-danger" href="manejadores/salir.php">
                      <i class="material-icons text-danger">&#xE879;</i> Salir </a>
                  </div>
                </li>
              </ul>
              <nav class="nav">
                <a href="#" class="nav-link nav-link-icon toggle-sidebar d-md-inline d-lg-none text-center border-left" data-toggle="collapse" data-target=".header-navbar" aria-expanded="false" aria-controls="header-navbar">
                  <i class="material-icons">&#xE5D2;</i>
                </a>
              </nav>
            </nav>
          </div>
            <?php } ?>
          <!-- / .main-navbar -->
          <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->


            <?php if($login){
              require("pages/modales/passmodal.php"); 
              ?>
            <!-- incicio -->
            <?php if(empty($_GET["page"])){ ?>
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">H2Obras</span>
                <h3 class="page-title">Resumen</h3>
              </div>
            </div>
            <!-- End Page Header -->
            <!-- Small Stats Blocks -->
            <div class="row">
              <div class="col-lg col-md-6 col-sm-6 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                  <div class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto">
                      <div class="stats-small__data text-center">
                        <span class="stats-small__label text-uppercase">Ventas</span>
                        <h6 class="stats-small__value count my-3">2.390</h6>
                      </div>
                      <div class="stats-small__data">
                        <span class="stats-small__percentage stats-small__percentage--increase">2.7%</span>
                      </div>
                    </div>
                    <canvas height="120" class="blog-overview-stats-small-1"></canvas>
                  </div>
                </div>
              </div>
              <div class="col-lg col-md-6 col-sm-6 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                  <div class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto">
                      <div class="stats-small__data text-center">
                        <span class="stats-small__label text-uppercase">productos vendidos</span>
                        <h6 class="stats-small__value count my-3">182</h6>
                      </div>
                      <div class="stats-small__data">
                        <span class="stats-small__percentage stats-small__percentage--increase">12.4%</span>
                      </div>
                    </div>
                    <canvas height="120" class="blog-overview-stats-small-2"></canvas>
                  </div>
                </div>
              </div>
              <div class="col-lg col-md-4 col-sm-6 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                  <div class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto">
                      <div class="stats-small__data text-center">
                        <span class="stats-small__label text-uppercase">INGRESOS</span>
                        <h6 class="stats-small__value count my-3">8,147</h6>
                      </div>
                      <div class="stats-small__data">
                        <span class="stats-small__percentage stats-small__percentage--decrease">3.8%</span>
                      </div>
                    </div>
                    <canvas height="120" class="blog-overview-stats-small-3"></canvas>
                  </div>
                </div>
              </div>
              <div class="col-lg col-md-4 col-sm-6 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                  <div class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto">
                      <div class="stats-small__data text-center">
                        <span class="stats-small__label text-uppercase">EGRESOS</span>
                        <h6 class="stats-small__value count my-3">2,413</h6>
                      </div>
                      <div class="stats-small__data">
                        <span class="stats-small__percentage stats-small__percentage--increase">12.4%</span>
                      </div>
                    </div>
                    <canvas height="120" class="blog-overview-stats-small-4"></canvas>
                  </div>
                </div>
              </div>
            </div>
            <!-- End Small Stats Blocks -->
            <div class="row">
              <!-- Users Stats -->
              <div class="col-lg-8 col-md-12 col-sm-12 mb-4">
                <div class="card card-small">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">VENTAS</h6>
                  </div>
                  <div class="card-body pt-0">
                    <div class="row border-bottom py-2 bg-light">
                      <div class="col-12 col-sm-6">
                        <div id="blog-overview-date-range" class="input-daterange input-group input-group-sm my-auto ml-auto mr-auto ml-sm-auto mr-sm-0" style="max-width: 350px;">
                          <input type="text" class="input-sm form-control" name="start" placeholder="Start Date" id="blog-overview-date-range-1">
                          <input type="text" class="input-sm form-control" name="end" placeholder="End Date" id="blog-overview-date-range-2">
                          <span class="input-group-append">
                            <span class="input-group-text">
                              <i class="material-icons"></i>
                            </span>
                          </span>
                        </div>
                      </div>
                      <div class="col-12 col-sm-6 d-flex mb-2 mb-sm-0">
                        <button type="button" class="btn btn-sm btn-white ml-auto mr-auto ml-sm-auto mr-sm-0 mt-3 mt-sm-0">Reporte Completo &rarr;</button>
                      </div>
                    </div>
                    <canvas height="130" style="max-width: 100% !important;" class="blog-overview-users"></canvas>
                  </div>
                </div>
              </div>
              <!-- End Users Stats -->
              <!-- Users By Device Stats -->
              <div class="col-lg-4 col-md-6 col-sm-12 mb-4">
                <div class="card card-small h-100">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Distribucion de Componentes Pedidos</h6>
                  </div>
                  <div class="card-body d-flex py-0">
                  <canvas height="220" class="blog-users-by-device m-auto"></canvas>
                  </div>
                  <div class="card-footer border-top">
                    <div class="row">
                      <div class="col">
                        <select class="custom-select custom-select-sm" style="max-width: 130px;">
                          <option selected>Semana Pasada</option>
                          <option value="1">Hoy</option>
                          <option value="2">Mes Pasado</option>
                          <option value="3">Año Pasado</option>
                        </select>
                      </div>
                      <div class="col text-right view-report">
                        <a href="#">Reporte Completo &rarr;</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- End Users By Device Stats -->
            </div>
            <?php } ?>
            <!-- FIN INICIO -->
            <!--VENTAS -->
            <?php if($_GET["page"]=="ven"){ 
                            require("pages/cotizador.php");
                            } ?>


            <!--FIN VENTAS -->
             <!--CLIENTES -->
             <?php if($_GET["page"]=="cli" && ($_SESSION["tipo"]=="admin")){ 
                            require("pages/user.php");
                            } ?>


            <!--FIN CLIENTES -->
            <!--DATOS DE USUARIO -->
             <?php if($_GET["page"]=="usr"){ 
                            require("pages/datos.php");
                            } ?>


            <!--FIN DATOS DE USUARIO -->

            <!-- CATALOGO -->
            <?php if($_GET["page"]=="cat" && ($_SESSION["tipo"]=="admin")){
              require("manejadores/tasa_cambio.php");
              require("pages/catalogo.php");
              ?>
                        <!-- Page Header -->
            
            <?php } ?>
            <!-- FIN CATALOGO -->
            <?php }else{ ?>
            <!-- Formulario Acceso -->
            <br/> 
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 mb-4">
                  <div class="card card-small">
                    <div class="card-header border-bottom">
                      <h6 class="m-0">ACCESO A SISTEMA</h6>
                    </div>
                    <div class="card-body pt-0">
                      <div class="row border-bottom py-2 bg-light">
                      <br/>
                      <br/>
                      </div>
                      <br/>
                      <form method="post" action="manejadores/login.php">
                        <div class="form-row">
                          <div class="form-group col-md-4">
                            <label for="usuario">Usuario:</label>
                            <input type="text" required class="form-control" name="usuario" id="usuario" placeholder="Usuario" value=""> </div>
                        </div>
                        <div class="form-row">
                          <div class="form-group col-md-4">
                          <label for="password">Password</label>
                            <input type="password" name="password" required  class="form-control" id="password" placeholder="Password"> </div>
                        </div>
                        <button type="submit" class="btn btn-accent">Aceptar</button>
                        <button type="button" class="btn btn-secondary" onclick="borrar()">Restablecer</button>
                        <br/>
                      <br/><br/>
                      <br/>
                        <script>
                          function borrar(){
                            document.getElementById("password").value="";
                            document.getElementById("usuario").value="";
                          }
                        </script>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <!-- End Formulario Acceso -->
            <?php } ?>
            





          </div>
          <footer class="main-footer d-flex p-2 px-3 bg-white border-top">
            <span class="copyright ml-auto my-auto mr-2">Copyright © 2019
              <a href="#" rel="nofollow">H2Obras SRL</a> v0.3.2
            </span>
          </footer>
        </main>
      </div>
    </div>
    <script src="scripts/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="scripts/Chart.min.js"></script>
    <script src="scripts/shards.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Sharrre/2.0.1/jquery.sharrre.min.js"></script>
    <script src="scripts/extras.1.0.0.min.js"></script>
    <script src="scripts/shards-dashboards.1.0.0.min.js"></script>
    <script src="scripts/app/app-blog-overview.1.0.0.js"></script>
  </body>
</html>