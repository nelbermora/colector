<script>
var status=0;
function scrollWin() {
  window.scrollTo(0, <?php echo $_GET["gety"];?>);
}
</script>
<div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">CATALOGO DE PRODUCTOS</h3>
              </div>
            </div>
            <!-- End Page Header -->
            <?php if($_GET["add"]=="ok"){ ?>
            <div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <i class="fa fa-check mx-2"></i>
            <strong>Carga Exitosa!</strong> Producto añadido! </div>
              <?php } ?>
            <div class="row">
              <div class="col-lg-8 mb-4">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">AÑADIR PRODUCTO</h6>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col-sm-12 col-md-12">
                          <strong class="text-muted d-block mb-2">Ingrese Datos de Producto</strong>
                          <form action="manejadores/addProducto.php" method="post">
                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <select id="inputState" class="form-control" name="producto" required>
                                    <option></option>                                  
                                  <?php $q1=$mysqli->query("select * from catalogo_maestro order by orden asc");
                                      while ($categoria_producto=$q1->fetch_object()){ ?>
                                      <option value="<?php echo $categoria_producto->id;?>"><?php echo $categoria_producto->descripcion;?></option>
                                    <?php } ?>
                                </select>  
                              
                              </div>
                              <div class="form-group col-md-6">
                                <input type="text" class="form-control" id="validationServer01" placeholder="Pulgadas" name="pulgadas" value="" required>
                              </div>
                            </div>
                             <div class="form-row">
                             
                             <div class="form-group col-md-6">
                                <input type="number"  class="form-control" id="validationServer02"  step="0.01" name="precio" placeholder="Precio Venta" required>
                              </div>
                              <div class="custom-control custom-toggle custom-toggle-sm mb-1">
                              <script>
                                      function CambiaLabel(){
                                        if(document.querySelector('#labelcambio').innerText == 'Moneda de Emisión: Dólar'){
                                          document.querySelector('#labelcambio').innerText = 'Moneda de Emisión: Pesos';
                                        }else{
                                          document.querySelector('#labelcambio').innerText = 'Moneda de Emisión: Dólar'
                                        }
                                        
                                        }
                                      
                                  </script>
                              <input type="checkbox" onChange="CambiaLabel();" checked id="customToggle1" name="dolar" class="custom-control-input">
                              <label class="custom-control-label" id="labelcambio" for="customToggle1">Moneda de Emisión: Dólar</label> 
                              </div> 
                            </div>
                            <div class="form-row">
                             
                              <div class="custom-control custom-toggle custom-toggle-sm mb-1">
                              <input type="checkbox" id="customToggle2" name="invariable" class="custom-control-input">
                              <label class="custom-control-label" for="customToggle2">Componente Invariable?</label><br/>
                              </div>
                            </div>
                            <div align="center">
                            <button type="submit" class="mb-2 btn btn-primary mr-2">Añadir</button>
                            </div>
                          </form>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="col-lg-4 mb-4">
                <!-- Sliders & Progress Bars -->
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">RESUMEN CATALOGO</h6>
                  </div>
                  <?php //info de cotizacion dolar
                  $co=$mysqli->query("SELECT * FROM `cotizaciones` order by id desc LIMIT 1");
                  $cotizacion=$co->fetch_object();
                  $valor=number_format($cotizacion->valor,4,",","."); 
                  $fecha_cotizacion=date('d/m/Y', strtotime($cotizacion->fecha_cotizacion)); 
                  ?>

                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">
                      <!-- Progress Bars -->
                      <div class="mb-6">
                        <strong class="text-muted d-block mb-3">Tasa de Cambio:</strong>
                        <table>
                        <tr>
                          <td>Valor:</td>
                          <td><input type="text" class="form-control" id="validationServer01" placeholder=<?php echo $valor;?> name="pulgadas" value="" disabled></td>
                        </tr>
                        <tr>
                          <td>Fecha:</td>
                          <td><input type="text" class="form-control" id="validationServer01" placeholder=<?php echo $fecha_cotizacion;?> disabled name="pulgadas" value="" required></td>
                        </tr>
                        <tr>
                          <td>Referencia:</td>
                          <td><input type="text" class="form-control" id="validationServer01"disabled  placeholder="Banco Nacion. Venta" name="pulgadas" value="" required></td>
                        </tr>
                        </table><br/>
                      </div>
                      
                      
                      
                      
                      <!-- / Progress Bars -->
                    </li>
                  </ul>
                </div>
                <!-- / Sliders & Progress Bars -->
              </div>
            </div>
            <div class="row">
              <div class="col">
                <div class="card card-small mb-4">
                 
                  
                  <div class="card-header border-bottom">
                    <h6 class="m-0">CATALOGO ACTUAL</h6>
                  </div>
                  <table class="table mb-0">
                      <thead class="bg-light">
                        <tr>
                          <th scope="col" class="border-0 text-center align-middle">#</th>
                          <th scope="col" width="200px" class="border-0 text-center align-middle">Producto</th>
                          <th scope="col" class="border-0 text-center align-middle">Pulgadas</th>
                          <th scope="col" class="border-0 text-center align-middle">Precio de Venta (Ars.)</th>
                          <th scope="col" class="border-0 text-center align-middle">Precio Cargado </th>
                          <th scope="col" class="border-0 text-center align-middle">Moneda </th>
                          <th scope="col" class="border-0 text-center align-middle">Tipo Componente</th>
                          <th scope="col" width="200px" class="border-0 text-center align-middle">Opciones</th>
                        </tr>
                      </thead>
                      
                      <?php 
                      $productos=$mysqli->query("select * from catalogo"); 
                      ?>
                      <form action="manejadores/modProducto.php" method="post" id="f1" name="f1">
                      <tbody>
                        <?php 
                        $cantidad=1;
                        while($vproductos=$productos->fetch_object()){ ?>
                        
                        <tr>
                          <td class="text-center align-middle"><?php echo $cantidad;?></td>
                          <td class="text-center align-middle"><?php echo $vproductos->producto;?></td>
                          
                          <td class="text-center align-middle" id=<?php echo "pulgada".$cantidad;?>><?php echo $vproductos->mayor;?></td>
                          
                          <td class="text-center align-middle"><?php echo "$ ".number_format($vproductos->precio,2,",",".");?></td>
                          <td class="text-center align-middle"id=<?php echo "precio".$cantidad;?>><?php if($vproductos->moneda_precio=="USD"){ echo $precio_referencia=$vproductos->precio_dolar;}else{ echo $precio_referencia=$vproductos->precio; }?> </td>
                          <td class="text-center align-middle"><?php echo $vproductos->moneda_precio;?></td>
                          <td class="text-center align-middle"><?php if($vproductos->preciobase==0){ echo "Variable";}else{echo "Invariable";};?></td>
                          <td class="text-center align-middle">
                          
                               
                                <button type="submit" style="display:none" class="btn btn-white"  id=<?php echo "guardar".$cantidad;?> onclick="">
                                    <span class="text-primary">
                                        <i class="material-icons">save</i>
                                    </span>
                                </button>
                                
                                <button type="button" class="btn btn-white" onclick="editaItem('<?php echo $cantidad;?>','<?php echo $vproductos->id;?>','<?php echo $vproductos->mayor;?>','<?php echo $precio_referencia;?>','<?php echo $vproductos->moneda_precio;?>');">
                                    <span class="text-success">
                                        <i class="material-icons">create</i>
                                    </span>  
                              </button>
                              <button type="button" class="btn btn-white" onclick="eliminaItem(<?php echo $vproductos->id;?>);">
                                    <span class="text-danger">
                                        <i class="material-icons">clear</i>
                                    </span>  
                               </button>
                               
                                                    
                           </td>
                        </tr>
                        
                      <?php $cantidad++; } ?>
                        
                      </tbody>
                                <input type="hidden" name="moneda_precio" id="moneda_precio">
                                <input type="hidden" name="id" id="id">
                                <input type="hidden" name="fila" id="fila">
                                <input type="hidden" name="medida" id="medida">
                                <input type="hidden" name="precio" id="precio">
                                <input type="hidden" name="locationy" id="locationy">
                                </form>
                    </table>
                    <script>
                    function eliminaItem(x){
                        var confirma=confirm("Desea Eliminar este Producto?")
                        if (confirma){
                        window.location.href='manejadores/delpresentacion.php?idp='+x;
                        }
                    }
                    function copiaMedida(valuemedida){
                        document.getElementById("medida").value=valuemedida;
                    }
                    function copiaPrecio(valueprecio){
                        document.getElementById("precio").value=valueprecio;
                    }
                    function editaItem(cantidad,idproducto,medida,precio,monedaprecio){
                        if(status==0){

                        
                        var celdaMedida=document.getElementById("pulgada"+cantidad);
                        var celdaPrecio=document.getElementById("precio"+cantidad);
                        celdaMedida.innerHTML="";
                        celdaPrecio.innerHTML="";
                        var inputMedida=document.createElement("INPUT");
                        inputMedida.setAttribute("type","text");
                        inputMedida.setAttribute("name",'inputMedida');
                        inputMedida.setAttribute("id",'inputMedida');
                        inputMedida.setAttribute("class","form-control col-6");
                        inputMedida.setAttribute("value",medida);
                        inputMedida.setAttribute("onchange",'copiaMedida(this.value)');
                        document.f1.appendChild(inputMedida);
                        celdaMedida.appendChild(inputMedida);
                        var inputPrecio=document.createElement("INPUT");
                        inputPrecio.setAttribute("type","text");
                        inputPrecio.setAttribute("class","form-control col-8");
                        inputPrecio.setAttribute("name",'inputPrecio');
                        inputPrecio.setAttribute("id",'inputPrecio');
                        inputPrecio.setAttribute("onchange",'copiaPrecio(this.value)');
                        inputPrecio.setAttribute("value",precio);

                        celdaPrecio.appendChild(inputPrecio);
                        var miboton=document.getElementById("guardar"+cantidad)
                        miboton.setAttribute("style","display");
                        document.getElementById("moneda_precio").value=monedaprecio;
                        document.getElementById("id").value=idproducto;
                        document.getElementById("fila").value=cantidad;
                        document.getElementById("medida").value=medida;
                        document.getElementById("precio").value=precio;
                        document.getElementById("locationy").value=window.scrollY;
                        status=1;
                        } else{
                            alert("Ya se encuentra editando otro elemento, debe guardar para continuar");
                        }  
                        
                        
                    }

                    
                    
                    </script>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>
            </div>
