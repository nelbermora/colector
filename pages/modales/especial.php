<form action="manejadores/send_confirmation_mail.php">
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Cotizaciones Especiales</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
                              <div class="form-row">
                                <div class="form-group col-md-12">
                                  <label for="descripcion">Descripcion:</label>
                                  <textarea class="form-control" id="descripcion" name="descripcion" rows="5"></textarea>
                                </div>
                              </div>
                              <div class="form-row">
                                <div class="form-group col-md-12">
                                  <label for="ciudad">Puede cargar algún archivo referencial (jpg,png,pdf,doc):</label>
                                  <input type="file" class="form-control-file" id="exampleFormControlFile1" accept="file_extension .gif, .jpg, .png, .doc, .docx"></div>
                              </div>
                              
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          <button type="submit" class="btn btn-primary">Enviar</button>
        </div>
      </div>
    </div>
  </div>
</form>