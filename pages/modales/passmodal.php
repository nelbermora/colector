<form action="manejadores/modPassword.php" method="post">
  <div class="modal fade" id="passModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Cambiar Contraseña</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
                              <div class="form-row">
                                <div class="form-group col-md-7">
                                  <label for="old">Contraeña Actual:</label>
                                  <input type="password" class="form-control" id="old" name="old"  required>
                                </div>
                              </div>
                              <div class="form-row">
                                <div class="form-group col-md-7">
                                  <label for="new">Nueva Contraseña:</label>
                                  <input type="password" class="form-control"  minlength="8" id="new" name="new" onchange="validaPass();" name="new" required>
                                </div>
                              </div>
                              <div class="form-row">
                                <div class="form-group col-md-7">
                                  <label for="new2">Confirmar Nueva Contraseña:</label>
                                  <input type="password" class="form-control" id="new2" name="new2" onchange="validaPass();" required>
                                  <label id="invalidpass" style="display:none" class="text-danger">&nbsp;Contraseñas no coinciden</label>
                                </div>
                              </div>
                              <script>
                              function validaPass(){
                                  pass1=document.getElementById('new').value;
                                  pass2=document.getElementById('new2').value;
                                  text=document.getElementById('invalidpass');
                                  boton=document.getElementById('botonenvia');
                                  if(pass1!=pass2){
                                      
                                      text.setAttribute("style","display");
                                      boton.setAttribute("disabled","");
                                  }else{
                                      text.setAttribute("style","display:none");
                                      boton.disabled=false;
                                  }
                              }
                              </script>
                                  
                              
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          <button type="submit" id="botonenvia" class="btn btn-primary">Enviar</button>
        </div>
      </div>
    </div>
  </div>
</form>