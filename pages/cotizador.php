<body onload="cargarCanos();">

<?php
              require("manejadores/tasa_cambio.php");
              require("pages/modales/especial.php");
              ?>
      <?php if($_GET["calc"]!="1"){ ?>            
         <!-- Page Header -->
         <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
              
                <h3 class="page-title">COTIZADOR COLECTOR</h3>
              </div>
            </div>
            <!-- End Page Header -->
            <div class="row">
              <div class="col-lg-9 col-md-12">
                <!-- Add New Post Form -->
                <form name="formu1" method="post" action="manejadores/addSuma.php">
                <div class="card card-small mb-3">
                <?php if($_GET["cot"]=="ok"){ ?>
                <div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
                <i class="fa fa-check mx-2"></i>
                <strong>Envio Exitoso!</strong> Se ha generado la orden de pedido especificado </div>
                <?php } ?>
                <div class="card-header border-bottom" id="focus">
                    <h6 class="m-0">Dimensiones y Diametro de Caño principal</h6>
                  </div>
                  <div class="card-body" id="">
                  
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Longitud Horizontal (mts):&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                          </div>
                          <input type="number" class="form-control" name="horizontal" min=0.5 max=3  step=0.01  required placeholder="Ingrese medidas" aria-label="Username1" aria-describedby="basic-addon1"> 
                          <div class="input-group-prepend">
                            <span class="input-group-text">Longitud Vertical (mts):&nbsp;&nbsp;&nbsp;&nbsp;</span>
                          </div>
                          <input type="number" class="form-control" name="vertical" min=0.6 max=1   step=0.01  required placeholder="Ingrese medidas" aria-label="Username" aria-describedby="basic-addon1">                         
                        </div>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text">DIAMETRO CAÑOS (Pulg.) &nbsp;&nbsp;</span>
                          </div>
                          <?php $productofppal=$mysqli->query("select * from catalogo where producto like '%caño%' and preciobase=0;"); 
                          ?>
                          <select required id="inputState" class="form-control" name="ppal" onChange="escribeVitaulic(this.selectedIndex)">
                                  <option value="">Elegir Diámetro...</option>
                                  <?php while($vproductof=$productofppal->fetch_object()){?>
                                  <option value="<?php echo $vproductof->id;?>"><?php echo $vproductof->mayor;?></option>
                                  <?php } ?>
                          </select>
                          <div class="input-group-prepend">
                            <span class="input-group-text">Acople Vitaulic (Pulg.) &nbsp;&nbsp;&nbsp;</span>
                          </div>
                          <?php $productofppal=$mysqli->query("select * from catalogo where producto like '%vitau%' and preciobase=0;"); 
                          ?>
                          <select disabled required id="vitaulic" class="form-control" name="vitaulic">
                                  <option value="">Medida Vitaulic..</option>
                                  <?php while($vproductof=$productofppal->fetch_object()){?>
                                  <option value="<?php echo $vproductof->id;?>"><?php echo $vproductof->mayor;?></option>
                                  <?php } ?>
                          </select>
                        </div>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Válvulas de Limpieza (Pulg.):</span>
                          </div>
                          <?php $productofppal=$mysqli->query("select * from catalogo where producto like '%limpieza%' and preciobase=0;"); 
                          ?>
                          <select required id="inputState" class="form-control" name="vavulimpieza">
                                  <option value="">Elegir Medida Valv. Limpieza...</option>
                                  <?php while($vproductof=$productofppal->fetch_object()){?>
                                  <option value="<?php echo $vproductof->id;?>"><?php echo $vproductof->mayor;?></option>
                                  <?php } ?>
                          </select>
                          <div class="input-group-prepend">
                            <span class="input-group-text">Válvulas Esféricas (Pulg.):</span>
                          </div>
                          <?php $productofppal=$mysqli->query("select * from catalogo where producto like '%esfericas%' and preciobase=0;"); 
                          ?>
                          <select required id="inputState" class="form-control" name="valvuesfe">
                                  <option value="">Elegir Medida Valv...</option>
                                  <?php while($vproductof=$productofppal->fetch_object()){?>
                                  <option value="<?php echo $vproductof->id;?>"><?php echo $vproductof->mayor;?></option>
                                  <?php } ?>
                          </select>
                        </div>
                     </div>
                 </div>
                          
                <div class="card card-small mb-3">
                <div class="card-header border-bottom" id="focus">
                    <h6 class="m-0">Parametrización de Salidas</h6>
                  </div>
                  <div class="card-body" id="">
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Cantidad de Salidas:</span>
                          </div>
                          <input type="number" class="form-control" name="salidas"   required placeholder="Ingrese cantidad" aria-label="Username1" aria-describedby="basic-addon1" onChange="genera_tabla(this.value)"> 
                        </div>
                        <?php      $productof=$mysqli->query("select * from catalogo where producto like '%caño%' and preciobase=0;");
                                   while($vproductof=$productof->fetch_object()){
                                   $medidascanos[]= $vproductof->mayor; }
                                   $productofniple=$mysqli->query("select * from catalogo where producto like '%niple%' and preciobase=0;");
                                   while($vproductofniple=$productofniple->fetch_object()){
                                   $medidasniples[]= $vproductofniple->mayor; }
                                   $productofesfe=$mysqli->query("select * from catalogo where producto like '%esfer%' and preciobase=0;"); 
                                   while($vproductofesfe=$productofesfe->fetch_object()){
                                    $medidasesfericas[]= $vproductofesfe->mayor;}
                                  $productofmari=$mysqli->query("select * from catalogo where producto like '%maripo%' and preciobase=0;"); 
                                   while($vproductofmari=$productofmari->fetch_object()){
                                    $medidasesmariposa[]= $vproductofmari->mayor;}
                                   
                                   
                                   ?>
                                                                  
                                    <script>
                                    function escribeVitaulic(myvalue){
                                      document.getElementById('vitaulic').selectedIndex=myvalue;
                                    }
                                    
                                    function cambiaValvula(valor,nombre){
                                      var iteracion=parseInt(nombre.substr(5,3));
                                      document.getElementById('valvula'+iteracion).value=valor;
                                    }
                                    
                                    
                                    var medidasniples =<?php echo json_encode($medidasniples);?>;
                                    
                                    function genera_tabla(x) {
                                      var miTabla = document.getElementById('tablita');
                                      var removeTab = document.getElementById('bodytablita');
                                      if(removeTab){
                                          var parentEl = removeTab.parentElement;
                                          parentEl.removeChild(removeTab);
                                      }
                                      var tblBody = document.createElement("tbody");
                                      for (var i = 0; i < x; i++) {
                                        var hilera = document.createElement("tr");
                                        for (var j = 0; j < 3; j++) {
                                          var celda = document.createElement("td");
                                          celda.setAttribute("class","align-middle");
                                            if(j==0){
                                                var textoCelda = document.createTextNode(i+1);
                                                celda.appendChild(textoCelda);
                                                celda.setAttribute("align","center");
                                            }else if(j==1){
                                                var selectNiple=document.createElement("SELECT");
                                                selectNiple.setAttribute("class", "form-control");
                                                selectNiple.setAttribute("required","");
                                                selectNiple.setAttribute("name","niple"+i);
                                                selectNiple.setAttribute("id","niple"+i);
                                                selectNiple.setAttribute("onChange","cambiaValvula(this.value,this.name)");
                                                var opcionini= document.createElement("OPTION");
                                                        opcionini.text="Elegir...";
                                                        opcionini.value="";
                                                        selectNiple.appendChild(opcionini);
                                                        
                                                    for (it=0;it<medidasniples.length;it++) { 
                                                        var opcion     = document.createElement("OPTION");
                                                        opcion.value=medidasniples[it];
                                                        opcion.text=medidasniples[it];
                                                        selectNiple.appendChild(opcion);
                                                    }
                                                celda.appendChild(selectNiple);
                                            }else{
                                                var selectValv=document.createElement("SELECT");
                                                selectValv.setAttribute("class", "form-control");
                                                selectValv.setAttribute("required","");
                                                selectValv.setAttribute("name","valvula"+i);
                                                selectValv.setAttribute("id","valvula"+i);
                                                selectValv.setAttribute("disabled","");
                                                selectValv.setAttribute("required","");
                                                var opcionini= document.createElement("OPTION");
                                                opcionini.value="";
                                                opcionini.text="Elegir...";
                                                selectValv.appendChild(opcionini);
                                                    for (it=0;it<medidasniples.length;it++) { 
                                                        var opcion     = document.createElement("OPTION");
                                                        opcion.value=medidasniples[it];
                                                        opcion.text=medidasniples[it];
                                                        selectValv.appendChild(opcion);
                                                    }
                                                celda.appendChild(selectValv);
                                                
                                            }
                                            
                                            
                                            hilera.appendChild(celda);  
                                        }
                                        tblBody.appendChild(hilera);
                                      }
                                      miTabla.appendChild(tblBody);
                                      tblBody.setAttribute("id", "bodytablita");
                                      
                                      
                                    }

                                   
                                    </script>
                        <table class="table mb-0" id="tablita">
                        <thead class="bg-light">
                        <tr>
                          <th class="text-center align-middle" scope="col" class="border-0">Salida #</th>
                          <th class="text-center align-middle" scope="col" class="border-0">Niples (Pulg.)</th>
                          <th class="text-center align-middle" scope="col" class="border-0">Válvulas (Pulg.)</th>
                          
                        </tr>
                        </thead>
                        
                        </table>
                        </div>
                        </a>
                            <div align="center"><button type="submit" class="mb-2 btn btn-primary mr-2">Cotizar</button></div>
                            <hr/>
                            <p align="center">
                            Para cotizaciones con otras especificaciones o condiciones especiales  
                            <a href="#exampleModal" data-toggle="modal" data-target="#exampleModal">Click acá!</a>
                            </p>
                            </div>             
                    
                            
                        </div>
                        
                         <!-- / Add New Post Form -->
              <div class="col-lg-3 col-md-12">
                <!-- Post Overview -->
                <div class='card card-small mb-3'>
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Longitud Horizontal:</h6>
                  </div>
                  <div class='card-body p-0'>
                    <ul class="list-group list-group-flush">
                      <li class="list-group-item p-3">
                        <span class="d-flex mb-2">
                        <i class="material-icons mr-1">vertical_align_bottom</i>          
                          <strong class="mr-1">Mínima:</strong>
                          <strong class="text-success">0,5 mts. (50 cms.)</strong>
                        </span>
                        <span class="d-flex mb-2">
                          <i class="material-icons mr-1">vertical_align_top</i>
                          <strong class="mr-1">Máxima:</strong>
                          <strong class="text-danger">3 mts. (300 cms.)</strong>
                        </span>
                    </ul>
                  </div>
                </div>
                <!-- / Post Overview -->
                <!-- Post Overview -->
                <div class='card card-small mb-3'>
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Longitud Vertical:</h6>
                  </div>
                  <div class='card-body p-0'>
                    <ul class="list-group list-group-flush">
                      <li class="list-group-item p-3">
                        <span class="d-flex mb-2">
                        <i class="material-icons mr-1">vertical_align_bottom</i>          
                          <strong class="mr-1">Mínima:</strong>
                          <strong class="text-success">0,6 mts. (60 cms.)</strong>
                        </span>
                        <span class="d-flex mb-2">
                          <i class="material-icons mr-1">vertical_align_top</i>
                          <strong class="mr-1">Máxima:</strong>
                          <strong class="text-danger">1 mts. (100 cms.)</strong>
                        </span>
                    </ul>
                  </div>
                </div>
                <!-- Post Overview -->
                <div class='card card-small mb-3'>
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Condiciones</h6>
                  </div>
                  <div class='card-body p-0'>
                    <ul class="list-group list-group-flush">
                      <li class="list-group-item px-3 pb-2">
                        <div class="custom-control custom-checkbox mb-1">
                          <input type="checkbox" required class="custom-control-input align-middle" id="category1">
                          <label class="custom-control-label align-middle" for="category1">Despacho y Entrega dentro de Rosario</label>
                        </div>
                      </li>
                      <?php /*
                      <li class="list-group-item d-flex px-3">
                        <button class="btn btn-sm btn-outline-accent">
                          <i class="material-icons">save</i> Save Draft</button>
                        <button class="btn btn-sm btn-accent ml-auto">
                          <i class="material-icons">file_copy</i> Publish</button>
                      </li> */ ?>
                    </ul>
                  </div>
                </div>
                <!-- / Post Overview -->
                </form>
              </div>
            </div>
             
      <?php } ?>
            <!-- Page Header -->
           
            <!-- End Page Header -->
            <br/>
            <!-- Page Resume -->
            <?php if($_GET["calc"]=="1"){ ?>
            <div class="row">
              
              <div class="col-lg-12 md-12">
                
                <!-- Input & Button Groups -->
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Total Cotización</h6>
                  </div>
                  <ul class="list-group list-group-flush">
                    <form onsubmit="return confirm('Desea generar Cotizacion?');" method="post" action="manejadores/send_confirmation_mail.php">
                    <li class="list-group-item px-3">
                        <!-- Input Groups -->
                        <input type="hidden" name="idcotizar" value="<?php echo $_GET["id"];?>">
                        <strong class="text-muted d-block mb-2">Datos Cliente</strong>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Nombre</span>
                          </div>
                          <input type="text" class="form-control" disabled placeholder="<?php echo $nombre_usuario;?>" aria-label="Username" aria-describedby="basic-addon1" value="<?php echo $nombre_usuario;?>"> 
                          <div class="input-group-prepend">
                            <span class="input-group-text">CUIT</span>
                          </div>
                          <input type="text" class="form-control" value="<?php echo $dni_usuario;?>" disabled aria-label="Username" aria-describedby="basic-addon1"> 
                        </div>
                          <div class="input-group mb-3 w-75">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Dirección:</span>
                          </div>
                          <input type="text" class="form-control"  value="<?php echo $direccion_usuario;?>" disabled aria-label="Username" aria-describedby="basic-addon1"> 
                        </div>
                        <div class="input-group mb-3 w-50">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Correo Electrónico:</span>
                          </div>
                          <input type="text" class="form-control"  value="<?php echo $email_usuario;?>" disabled aria-label="Username" aria-describedby="basic-addon1" > 
                        </div>
                    </li>
                        <li class="list-group-item px-3">
                        <div class="input-group mb-3">
                        <div class="input-group mb-3 w-25">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Longitud Horizontal:</span>
                          </div>
                          <input type="text" class="form-control" disabled aria-label="Username" aria-describedby="basic-addon1"  value="<?php echo $_GET["hor"]." cms.";?>"> 
                        </div>
                        <div class="input-group mb-3 w-25">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Longitud Vertical:</span>
                          </div>
                          <input type="text" class="form-control" disabled  value="<?php echo $_GET["ver"]." cms.";?>"> 
                        </div>
                        </div>
                        <div class="input-group mb-3">
                        <table class="table mb-0 table-fixed" id="tablita">
                        <thead class="bg-light">
                        <tr>
                          <th class="text-center" width="85" scope="col" class="border-0">Salida #</th>
                          <th class="text-center" width="160" scope="col" class="border-0">Niples</th>
                          <th class="text-center" scope="col" class="border-0">Válvulas</th>
                          <th class="text-center" width="160" scope="col" class="border-0">Subtotal</th>
                          
                        </tr>
                        </thead>
                        <?php for($i=0;$i<$_GET["qty"];$i++){ ?>
                          <tr>
                          <td class="text-center"><?php echo $i+1;?></td>
                          <td class="text-center"><?php  if($men=urldecode($_GET["niple".$i])){echo $men."”"; }else{echo "--";};?></td>
                          <td class="text-center"><?php  if($men=urldecode($_GET["niple".$i])){echo $men."”"; }else{echo "--";};?></td>
                          <td class="text-center" scope="col"><?php echo number_format(floatval($_GET["pricesal".$i]),2,",",".");?></td>
                          </tr>
                        <?php }
                        ?>
                        <tr>
                          <td class="text-center">--</td>
                          <td class="text-left" colspan="2"><?php echo "Costo de Cañeria Principal de ".$_GET["ver"]." por ".$_GET["hor"]." cms. de ".urldecode($_GET["ppal"])." pulgadas"; ?></td>
                          <td class="text-center" scope="col"><?php echo number_format(floatval($_GET["preppal"]),2,",",".");?></td>
                        </tr>
                        <thead class="bg-light">
                        <tr>
                          <th class="text-right" scope="col" class="border-0" colspan="3">TOTAL</th>
                          <th class="text-center" scope="col" class="border-0"><?php echo "$ ".number_format($_GET["total"],2,",",".");?></th>
                        </tr>
                        </thead>
                        </table>
                        </div>
                            <?php if($_GET["total"]>0){ ?>
                            <div align="center"><button type="submit" class="mb-2 btn btn-primary mr-2">Generar</button></div>
                            <?php } ?>
                            </form>
                        <!-- Input Groups -->
                    </li>
                  </ul>
                </div>
                <!-- / Input & Button Groups -->
              </div>
            </div>
                            <?php } ?>
            <!-- Page Resume -->
                            