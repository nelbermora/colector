<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>H2Obras - Solicitud Cotización</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">	
		<tr>
			<td style="padding: 10px 0 30px 0;">
				<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border: 1px solid #cccccc; border-collapse: collapse;">
					<tr>
						<td align="center" bgcolor="" style="padding: 40px 0 30px 0; color: #153643; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
							<img src="../images/h2obras.jpg" alt="H2Obras" style="display: block;" />
						</td>
					</tr>
					<tr>
						<td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								
								<div class="row">
                                            <div class="col-md-6">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th style="text-align: center" colspan="4" scope="col">DATOS CLIENTE</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <th scope="col">Nombre</th>
                                                            <td colspan="3">'.$nombre_usuario.'</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="col">DNI/CUIT</th>
                                                            <td colspan="3">'.$dni_usuario.'</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="col">Email</th>
                                                            <td colspan="3">'.$email_usuario.'</td>
                                                        </tr>
                                                    </tbody>
                                                    <thead>
                                                        <tr>
                                                            <th style="text-align: center" colspan="4" scope="col">DATOS COTIZACIÓN</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <th colspan="2" scope="col">Fecha Cotización:</th>
                                                            <td colspan="2" style="text-align: center">'.$fecha.'</td>
                                                        </tr>
                                                        <tr>
                                                            <th colspan="2" scope="col">Monto Total Cotización:</th>
                                                            <td colspan="2" style="text-align: center">'.$monto_total.'</td>
                                                        </tr><tr>
                                                            <th colspan="2" scope="col">Cantidad de Colectores:</th>
                                                            <td colspan="2" style="text-align: center">1</td>
                                                        </tr>
                                                    </tbody>
                                                    <thead>
                                                        <tr>
                                                            <th style="text-align: center" colspan="4" scope="col">ESTRUCTURA</th>
                                                        </tr>
                                                        <tr>
                                                            <th style="text-align: center" colspan="4">Dimensiones</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <th scope="col">Horizontal:</th>
                                                            <td style="text-align: center">'.$longitud_horizontal.' mts</td>
                                                            <th scope="col">Vertical:</th>
                                                            <td style="text-align: center">'.$longitud_vertical.' mts</td>
                                                        </tr>
                                                    </tbody>
                                                    <thead>
                                                        <tr>
                                                            <th style="text-align: center" colspan="4">Medidas de Cañería Principal</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <th scope="col">Diámetro de Caño:</th>
                                                            <td style="text-align: center">'.$canio.'"</td>
                                                            <th scope="col">Acople Vitaulic:</th>
                                                            <td style="text-align: center">'.$acople_vit.'"</td>
                                                        </tr>
                                                    </tbody>
                                                    <thead>
                                                        <tr>
                                                            <th style="text-align: center" colspan="4">Medidas de Válvulas</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <th scope="col">Limpieza:</th>
                                                            <td style="text-align: center">'.$val_limpieza.'"</td>
                                                            <th scope="col">Esféricas:</th>
                                                            <td style="text-align: center">'.$val_esfe.'"</td>
                                                        </tr>
                                                    </tbody>
                                                    <thead>
                                                        <tr>
                                                            <th style="text-align: center" colspan="4">Salidas</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <th style="text-align: center" scope="col" colspan="2">Cantidad:</th>
                                                            <td style="text-align: center" colspan="2">'.$cant_salidas.'</td>
                                                        </tr>';
                                                        $qu=$mysqli->query("select * from cotizar_detalle where id_cotizar='$id_cotizar' order by id asc");
                                                        while($salidas=$qu->fetch_object()){
                                                        $qn=$mysqli->query("select mayor from catalogo where id='$salidas->niple'");
                                                        $niple=$qn->fetch_object();
                                                        $qv=$mysqli->query("select mayor from catalogo where id='$salidas->valvula'");
                                                        $valvula=$qv->fetch_object();
                                                        $mensaje1=$mensaje1.'
                                                        <tr>
                                                        <td scope="col">Medida Niple:</td>
                                                        <td style="text-align: center">'.$niple->mayor.'"</td>
                                                        <td scope="col">Madida Válvula:</td>
                                                        <td style="text-align: center">'.$valvula->mayor.'"</td>
                                                        </tr>';
                                                        }
$mensaje2='
                                                        </tbody>
                                                </table>
                                            </div>
                                    </div>
                                
						</td>
					</tr>
					<tr>
						<td bgcolor="#70bbd9" style="padding: 30px 30px 30px 30px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;" width="75%">
										&reg; H2Obras, Argentina 2019<br/>
										
									</td>
									<td align="right" width="25%">
										<table border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td style="font-family: Arial, sans-serif; font-size: 12px; font-weight: bold;">
													<a href="#" style="color: #ffffff;">
														<img src="images/tw.gif" alt="Twitter" width="38" height="38" style="display: block;" border="0" />
													</a>
												</td>
												<td style="font-size: 0; line-height: 0;" width="20">&nbsp;</td>
												<td style="font-family: Arial, sans-serif; font-size: 12px; font-weight: bold;">
													<a href="#" style="color: #ffffff;">
														<img src="images/fb.gif" alt="Facebook" width="38" height="38" style="display: block;" border="0" />
													</a>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>';






















<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <div class="row">
        <div class="col-md-6">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th style="text-align: center" colspan="4" scope="col">DATOS CLIENTE</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="col">Nombre</th>
                        <td colspan="3">Soluciones Refe SRL CA</td>
                    </tr>
                    <tr>
                        <th scope="col">DNI/CUIT</th>
                        <td colspan="3">20-95933434-0</td>
                    </tr>
                    <tr>
                        <th scope="col">Email</th>
                        <td colspan="3">info@solurefe.com.ve</td>
                    </tr>
                </tbody>
                <thead>
                    <tr>
                        <th style="text-align: center" colspan="4" scope="col">DATOS COTIZACIÓN</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th colspan="2" scope="col">Fecha Cotización:</th>
                        <td colspan="2" style="text-align: center">25/03/2019</td>
                    </tr>
                    <tr>
                        <th colspan="2" scope="col">Monto Total Cotización:</th>
                        <td colspan="2" style="text-align: center">$ 29.999,88</td>
                    </tr><tr>
                        <th colspan="2" scope="col">Cantidad de Colectores:</th>
                        <td colspan="2" style="text-align: center">1</td>
                    </tr>
                </tbody>
                <thead>
                    <tr>
                        <th style="text-align: center" colspan="4" scope="col">ESTRUCTURA</th>
                    </tr>
                    <tr>
                        <th style="text-align: center" colspan="4">Dimensiones</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="col">Horizontal:</th>
                        <td style="text-align: center">0,56 mts</td>
                        <th scope="col">Vertical:</th>
                        <td style="text-align: center">1,56 mts</td>
                    </tr>
                </tbody>
                <thead>
                    <tr>
                        <th style="text-align: center" colspan="4">Medidas de Cañería Principal</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="col">Diámetro de Caño:</th>
                        <td style="text-align: center">1 2/4"</td>
                        <th scope="col">Acople Vitaulic:</th>
                        <td style="text-align: center">1 2/4"</td>
                    </tr>
                </tbody>
                <thead>
                    <tr>
                        <th style="text-align: center" colspan="4">Medidas de Válvulas</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="col">Limpieza:</th>
                        <td style="text-align: center">1 2/4"</td>
                        <th scope="col">Esféricas:</th>
                        <td style="text-align: center">1 2/4"</td>
                    </tr>
                </tbody>
                <thead>
                    <tr>
                        <th style="text-align: center" colspan="4">Salidas</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th style="text-align: center" scope="col" colspan="2">Cantidad:</th>
                        <td style="text-align: center" colspan="2">4</td>
                    </tr>
                    <tr>
                        <td scope="col">Medida Niple:</td>
                        <td style="text-align: center">1 2/4"</td>
                        <td scope="col">Madida Válvula:</td>
                        <td style="text-align: center">1 2/4"</td>
                    </tr>
                    <tr>
                        <td scope="col">Medida Niple:</td>
                        <td style="text-align: center">1 2/4"</td>
                        <td scope="col">Madida Válvula:</td>
                        <td style="text-align: center">1 2/4"</td>
                    </tr>
                    <tr>
                        <td scope="col">Medida Niple:</td>
                        <td style="text-align: center">1 2/4"</td>
                        <td scope="col">Madida Válvula:</td>
                        <td style="text-align: center">1 2/4"</td>
                    </tr>
                    <tr>
                        <td scope="col">Medida Niple:</td>
                        <td style="text-align: center">1 2/4"</td>
                        <td scope="col">Madida Válvula:</td>
                        <td style="text-align: center">1 2/4"</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>