<?php require("pages/modales/passmodal.php"); ?>
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
 
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                
                <h3 class="page-title">Modificación de Datos</h3>
              </div>
            </div>
            <!-- End Page Header -->
            <?php if($_GET["add"]=="ok"){ ?>
            <div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <i class="fa fa-check mx-2"></i>
            <strong>Carga Exitosa!</strong> Usuario añadido! </div>
              <?php } ?>
              <?php if($_GET["mod"]=="ok"){ ?>
            <div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <i class="fa fa-check mx-2"></i>
            Modificacion Cargada </div>
              <?php } ?>
              <?php if($_GET["add"]=="error"){ ?>
            <div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <i class="fa fa-check mx-2"></i>
            <strong>Error en carga!</strong> Direccion de Correo electronica ya esta registrada. </div>
              <?php } ?>
            <!-- Default Light Table -->
            <div class="row">
              <div class="col-lg-8">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0" id="titulo">Mis datos</h6>
                  </div>
                  <?php $q1=$mysqli->query("select * from clientes c, usuarios u where c.id=$_SESSION[id] and u.id_clientes=c.id");
                        $datos=$q1->fetch_object();
                        ?>
                  <form method="post" action="manejadores/modUser.php" id="formu1">
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">
                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="nombre">Nombre:</label>
                                <input type="text" required class="form-control" name="nombre" id="nombre" placeholder="Nombre" value="<?php echo $datos->nombre;?>"> </div>
                              <div class="form-group col-md-6">
                                <label for="apellido">Apellido:</label>
                                <input type="text" class="form-control" id="apellido" name="apellido" placeholder="Apellido" value="<?php echo $datos->apellido;?>"> </div>
                            </div>
                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="dni">CUIT/CUIL/DNI:</label>
                                <input type="text" required class="form-control" id="dni" name="dni" placeholder="Nro Documento" value="<?php echo $datos->cuit;?>"> </div>
                                <div class="form-group col-md-6">
                                <label for="usuario">Usuario:</label>
                                <input type="text" required class="form-control" name="usuario" id="usuario" placeholder="Usuario" value="<?php echo $datos->usuario;?>"> </div>
                            </div>
                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="email">Email:</label>
                                <input type="email" required class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $datos->email;?>"> </div>
                              <div class="form-group col-md-6">
                                <label for="ciudad">Ciudad:</label>
                                <input type="text" class="form-control" id="ciudad" name="ciudad" placeholder="Ciudad" value="<?php echo $datos->ciudad;?>"> </div>
                            </div>
                            <div class="form-group">
                              <label for="direccion">Dirección:</label>
                              <input type="text" name="direccion" class="form-control" id="direccion" placeholder="Dirección" value="<?php echo $datos->direccion;?>"> 
                            </div>
                            <div align="center">
                              <button type="button" id="botonvolver" style="display:none" onclick="restablecer();" class="btn btn-accent">Volver</button>
                              <button type="submit" id="botonguardar" class="btn btn-accent">Modificar</button>
                            </div>
                        </div>
                      </div>
                    </li>
                  </ul>
                 </form>
                </div>
              </div>
              <div class="col-lg-4 mb-4">
                <!-- Sliders & Progress Bars -->
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Información Adicional</h6>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">
                      <!-- Progress Bars -->
                      <div class="mb-2">
                        <strong class="text-muted d-block mb-3">Último Acceso:</strong>
                        <label><?php echo date("d/m/Y H:i:s",strtotime($datos->last_login));?></label>
                      </div>
                      <div class="mb-2">
                        <strong class="text-muted d-block mb-3">Última Modificación:</strong>
                        <label><?php echo date("d/m/Y H:i:s",strtotime($datos->modified));?></label>
                      </div>
                      <div class="mb-2">
                        <strong class="text-muted d-block mb-3">Fecha Creación:</strong>
                        <label><?php echo date("d/m/Y H:i:s",strtotime($datos->created)); ?></label>
                      </div>
                      <!-- / Progress Bars -->
                    </li>
                    <li class="list-group-item p-3">
                      <div class="mb-2">
                        <strong class="text-muted d-block mb-3">
                            <a href="#passModal" data-toggle="modal" data-target="#passModal">
                                Click Aqui para reiniciar Password
                            </a>
                        </strong>
                      </div>
                    </li>
                  </ul>
                </div>
                <!-- / Sliders & Progress Bars -->
               </div>
            </div>
            <!-- End Default Light Table -->