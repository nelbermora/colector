 <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
 
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                
                <h3 class="page-title">GESTION DE CLIENTES</h3>
              </div>
            </div>
            <!-- End Page Header -->
            <?php if($_GET["add"]=="ok"){ ?>
            <div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <i class="fa fa-check mx-2"></i>
            <strong>Carga Exitosa!</strong> Usuario añadido! </div>
              <?php } ?>
              <?php if($_GET["mod"]=="ok"){ ?>
            <div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <i class="fa fa-check mx-2"></i>
            Modificacion Cargada </div>
              <?php } ?>
              <?php if($_GET["add"]=="error"){ ?>
            <div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <i class="fa fa-check mx-2"></i>
            <strong>Error en carga!</strong> Direccion de Correo electronica ya esta registrada. </div>
              <?php } ?>
            <!-- Default Light Table -->
            <div class="row">
              <div class="col-lg-5">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom text-center">
                    <h4 class="mb-0">Clientes Registrados</h4>
                  </div>
                  <script>
                  function muestraDatos(x){
                   document.getElementById('nombre').value=miArray[x][1];
                   document.getElementById('apellido').value=miArray[x][2];
                   document.getElementById('dni').value=miArray[x][3];
                   if(miArray[x][4]=="j"){
                    document.getElementById('tipo').value="j";
                   }else{
                    document.getElementById('tipo').value="f";
                   }
                   //
                   document.getElementById('usuario').value=miArray[x][5];
                   document.getElementById('password').value=miArray[x][6];
                   document.getElementById('password').setAttribute("type","password");
                   document.getElementById('email').value=miArray[x][7];
                   document.getElementById('ciudad').value=miArray[x][8];
                   document.getElementById('direccion').value=miArray[x][9];
                   document.getElementById('descripcion').value=miArray[x][10];
                   document.getElementById('botonguardar').innerHTML="Modificar";
                   document.getElementById('botonguardar').setAttribute("class","btn btn-warning");
                   document.getElementById('botonvolver').setAttribute("style","display");
                   document.getElementById('formu1').setAttribute("action","manejadores/modCliente.php");
                   document.getElementById('hidden').value=miArray[x][0];
                   document.getElementById('escondido').setAttribute("style","display");
                   document.getElementById('titulo').innerHTML="Detalle de Cliente";
                   if(miArray[x][11]=="1"){
                    document.getElementById('activo').checked=true;
                    
                   }else{
                    document.getElementById('activo').checked=false;
                   
                   }
                   
                   
                   
                  }
                  function restablecer(){
                   document.getElementById('nombre').value=null
                   document.getElementById('apellido').value=null;
                   document.getElementById('dni').value=null;
                   document.getElementById('tipo').value=null;
                   document.getElementById('usuario').value=null;
                   document.getElementById('password').value=null;
                   document.getElementById('password').setAttribute("type","text");
                   document.getElementById('email').value=null;
                   document.getElementById('ciudad').value=null;
                   document.getElementById('direccion').value=null;
                   document.getElementById('descripcion').value=null;
                   document.getElementById('botonguardar').innerHTML="Guardar Nuevo";
                   document.getElementById('botonguardar').setAttribute("class","btn btn-accent");
                   document.getElementById('botonvolver').setAttribute("style","display:none");
                   document.getElementById('formu1').setAttribute("action","manejadores/addCliente.php");
                   document.getElementById('hidden').value=null;
                   document.getElementById('escondido').setAttribute("style","display:none");
                   document.getElementById('titulo').innerHTML="Añadir nuevo Cliente";
                   
                   
                   
                  }
                  </script>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-4">
                    <div class="pre-scrollable" style="max-height: 70vh">
                    <table class="table mb-0 table-fixed table-hover" id="tablita">
                        <thead class="bg-light">
                        <tr>
                          <th class="text-center"  scope="col" class="border-0">Cliente</th>
                          <th class="text-center" scope="col" class="border-0">Email</th>
                          <th class="text-center" scope="col" class="border-0">Activo</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php 
                        $q=$mysqli->query("select c.id,c.nombre,c.apellido,c.cuit,c.tipo,u.usuario,u.password,u.activo,c.email,c.ciudad,c.direccion,c.descripcion from clientes c, usuarios u where u.id_clientes=c.id;");
                        $contador=0;
                        $usuarios=array();
                        while($user=$q->fetch_object()){
                        $usuarios[]=array($user->id,$user->nombre,$user->apellido,$user->cuit,$user->tipo,$user->usuario,$user->password,$user->email,$user->ciudad,$user->direccion,$user->descripcion,$user->activo);
                        /*,*/                                ?>
                                <tr>
                                <td style="user-select: none;" onclick="muestraDatos(<?php echo $contador;?>);"><?php echo $user->nombre." ".$user->apellido;?></td>
                                <td style="user-select: none;" onclick="muestraDatos(<?php echo $contador;?>);"><?php echo $user->email;?></td>
                                <td style="user-select: none;" onclick="muestraDatos(<?php echo $contador;?>);"><?php if($user->activo=="1"){echo "Si";}else{echo "No";};?></td>
                                </tr>
                        <?php 
                        $contador++;
                        } 
                        ?>
                        <script>var miArray=<?php echo json_encode($usuarios);?>;</script>
                        <tbody>
                    </table>
                    </div>
                    </li>
                    <?php ?>
                    <li class="list-group-item p-4">
                    <strong class="text-muted d-block mb-2"><i class="material-icons">
                      info
                    </i> Seleccione un cliente de la lista</strong>
                    </li>
                    
                  </ul>
                </div>
              </div>
              <div class="col-lg-7">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0" id="titulo">Añadir nuevo cliente</h6>
                  </div>
                  <form method="post" action="manejadores/addCliente.php" id="formu1">
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">
                          <form>
                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="nombre">Nombre:</label>
                                <input type="text" required class="form-control" name="nombre" id="nombre" placeholder="Nombre" value=""> </div>
                              <div class="form-group col-md-6">
                                <label for="apellido">Apellido:</label>
                                <input type="text" class="form-control" id="apellido" name="apellido" placeholder="Apellido" value=""> </div>
                            </div>
                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="dni">CUIT/CUIL/DNI:</label>
                                <input type="text" required class="form-control" id="dni" name="dni" placeholder="Nro Documento" value=""> </div>
                              <div class="form-group col-md-6">
                              <label for="tipo">Tipo de Cliente</label>
                              <select name="tipo" id="tipo" required  class="form-control">
                                  <option>Elegir...</option>
                                  <option value="j">Juridico</option>
                                  <option value="f">Fisico</option>
                                </select></div>
                            </div>
                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="usuario">Usuario:</label>
                                <input type="text" required class="form-control" name="usuario" id="usuario" placeholder="Usuario" value=""> </div>
                              <div class="form-group col-md-6">
                              <label for="password">Password</label>
                                <input type="text" name="password" required  class="form-control" id="password" placeholder="Password"> </div>
                            </div>
                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="email">Email:</label>
                                <input type="email" required class="form-control" name="email" id="email" placeholder="Email" value=""> </div>
                              <div class="form-group col-md-6">
                                <label for="ciudad">Ciudad:</label>
                                <input type="text" class="form-control" id="ciudad" name="ciudad" placeholder="Ciudad" value=""> </div>
                            </div>
                            <div class="form-group">
                              <label for="direccion">Dirección:</label>
                              <input type="text" name="direccion" class="form-control" id="direccion" placeholder="Dirección"> 
                            </div>
                            <div class="form-row">
                              <div class="form-group col-md-12">
                                <label for="descripcion">Descripcion</label>
                                <textarea class="form-control" id="descripcion" name="descripcion" rows="5"></textarea>
                              </div>
                            </div>
                            <div class="form-row" id="escondido" style="display:none">
                              <div class="form-group col-md-12">
                              <div class="custom-control custom-toggle custom-toggle-sm mb-1">
                              <input type="checkbox"  id="activo" name="activo" class="custom-control-input">
                              <label class="custom-control-label" id="labelcambio" for="activo">Usuario Activo?</label> 
                              </div> 
                              </div>
                            </div>
                            <input type="hidden" name="idcliente" id="hidden"/>
                            <div align="center">
                              <button type="button" id="botonvolver" style="display:none" onclick="restablecer();" class="btn btn-accent">Volver</button>
                              <button type="submit" id="botonguardar" class="btn btn-accent">Guardar Nuevo</button>
                            </div>
                          </form>
                        </div>
                      </div>
                    </li>
                  </ul>
                 </form>
                </div>
              </div>
            </div>
            <!-- End Default Light Table -->