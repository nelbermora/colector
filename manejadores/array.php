<?php
/* En la captura de tu pregunta aparenta estar definido así 'error' */
$error = [ 
'0' => '1',
'1' => '2',
'2' => '3'
 ];
$error = serialize($error);
$error = base64_encode($error);
$error = urlencode($error);
/* OJO: agregamos 'error=' para que en el otro lado llegue como $_GET['mensaje'] */
header("Location: receptor.php?mensaje=".$error);