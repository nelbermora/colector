<?php
include("conexion/conec.php");
session_start();
date_default_timezone_set('America/Buenos_Aires');


if (date("D",strtotime(date("Y-m-d")))=="Mon"){
    $dia=date("d")-3;
    $mes=date("m");
    $anio=date("Y");
    $fecha_cotizacion=date("Y-m-d",strtotime(date($anio."-".$mes."-".$dia)));

}elseif(date("D",strtotime(date("Y-m-d")))=="Sun"){
    $dia=date("d")-2;
    $mes=date("m");
    $anio=date("Y");
    $fecha_cotizacion=date("Y-m-d",strtotime(date($anio."-".$mes."-".$dia)));
}
else{
    $dia=date("d")-1;
    $mes=date("m");
    $anio=date("Y");
    $fecha_cotizacion=date("Y-m-d",strtotime(date($anio."-".$mes."-".$dia)));
}

$q=$mysqli->query("select valor from cotizaciones where fecha_cotizacion='$fecha_cotizacion'");
$valida=$q->fetch_object();
$valor=$valida->valor;
if(empty($valor)){
$opts = array('http'=>array('header' => "User-Agent:MyAgent/1.0\r\n"));
$context = stream_context_create($opts);
$header = file_get_contents("http://www.bna.com.ar/Cotizador/HistoricoPrincipales?id=billetes&fecha=$dia%2F$mes%2F$anio&filtroEuro=0&filtroDolar=1",false,$context);
$longitud_cadena=strlen($header);
    if($dia<10 && $mes<10){
        
        /*******este se usa para fechas de 6 digitos***********/
        $tasa_venta=str_replace(",",".",substr($header,$longitud_cadena-212,7));
    }elseif(($dia<10 && $mes>9) or ($dia>9 && $mes<10)) {
        
        /*******este se usa para fechas de 7 digitos***********/
        $tasa_venta=str_replace(",",".",substr($header,$longitud_cadena-213,7));
    }else{
        
        /*******este se usa para fechas de 8 digitos***********/
        $tasa_venta=str_replace(",",".",substr($header,$longitud_cadena-214,7));
    }
$cambio=(float)$tasa_venta;
if($cambio>0){
$fecha_carga=date("Y/m/d H:m:s");
$mysqli->query("INSERT INTO cotizaciones (fecha_cotizacion,fecha_carga,valor) values 
            ('$fecha_cotizacion','$fecha_carga','$cambio' )");
$mysqli->query("update `catalogo` set `precio`=precio_dolar*'$cambio' where moneda_precio='USD'");
}
else{
//mecanismo a activarse si falla la cotizacion
//Enviar Mail a Soporte informando la novedad
echo "ERROR EN CALCULO DE PRECIOS";
}
}
?>