<?php
include("../conexion/conec.php");
session_start();
include("../conexion/globales/globals.php");
$id_cotizar=$_POST["idcotizar"];
$q=$mysqli->query("select * from cotizar where id='$id_cotizar'");
$solicitud=$q->fetch_object();
$fecha=$solicitud->fecha;	
$monto_total=$solicitud->monto_total;	
$longitud_horizontal=$solicitud->longitud_horizontal;	
$longitud_vertical=$solicitud->longitud_vertical;	
$canio=$solicitud->canio;	
$acople_vit=$solicitud->acople_vit;
$val_limpieza=$solicitud->val_limpieza;
$val_esfe=$solicitud->val_esfe;
$cant_salidas=$solicitud->cant_salidas;
$ql=$mysqli->query("select mayor from catalogo where id='$val_limpieza'");
$limpieza=$ql->fetch_object();
$val_limpieza=$limpieza->mayor;
$qe=$mysqli->query("select mayor from catalogo where id='$val_esfe'");
$esferica=$qe->fetch_object();
$val_esfe=$esferica->mayor;   
$qc=$mysqli->query("select mayor from catalogo where id='$canio'");
$can=$qc->fetch_object();
$canio=$can->mayor;
$qvi=$mysqli->query("select mayor from catalogo where id='$acople_vit'");
$vitau=$qvi->fetch_object();
$acople_vit=$vitau->mayor;

// Varios destinatarios
$para  = $email_usuario.",".$email_admin; // atenci&oacuten a la coma

// titulo
$titulo = 'Solicitud de Cotizacion Ejecutada';

// mensaje
$mensaje1 = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>H2Obras - Solicitud Cotizaci&oacuten</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">	
		<tr>
			<td style="padding: 10px 0 30px 0;">
				<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border: 1px solid #cccccc; border-collapse: collapse;">
					<tr>
						<td align="center" bgcolor="" style="padding: 40px 0 30px 0; color: #153643; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
							<img src="http://h2obras.com/colector/images/h2obras.jpg" alt="H2Obras" style="display: block;" />
						</td>
					</tr>
					<tr>
						<td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								
								
                                                <table width="100%" style="border: 1px solid #cccccc; border-collapse: collapse;">
                                                    <thead>
                                                        <tr>
                                                            <th style="text-align: center" colspan="4" scope="col">DATOS CLIENTE</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <th scope="col" style="border: solid 1px #cccccc;">Nombre</th>
                                                            <td colspan="3" style="border: solid 1px #cccccc;">'.$nombre_usuario.'</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="col" style="border: solid 1px #cccccc;">DNI/CUIT</th>
                                                            <td colspan="3" style="border: solid 1px #cccccc;">'.$dni_usuario.'</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="col" style="border: solid 1px #cccccc;">Email</th>
                                                            <td colspan="3" style="border: solid 1px #cccccc;">'.$email_usuario.'</td>
                                                        </tr>
                                                    </tbody>
                                                    <thead>
                                                        <tr>
                                                            <th colspan="4" scope="col" style="text-align: center; border: solid 1px #cccccc;">DATOS COTIZACI&oacuteN</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <th colspan="2" scope="col" style="border: solid 1px #cccccc;">Fecha Cotizaci&oacuten:</th>
                                                            <td colspan="2" style="text-align: center; border: solid 1px #cccccc;">'.$fecha.'</td>
                                                        </tr>
                                                        <tr>
                                                            <th colspan="2" scope="col" style="border: solid 1px #cccccc;">Monto Total Cotizaci&oacuten:</th>
                                                            <td colspan="2" style="text-align: center; border: solid 1px #cccccc;">'.number_format($monto_total,2,",",".").'</td>
                                                        </tr><tr>
                                                            <th colspan="2" scope="col" style="border: solid 1px #cccccc;">Cantidad de Colectores:</th>
                                                            <td colspan="2" style="text-align: center; border: solid 1px #cccccc;">1</td>
                                                        </tr>
                                                    </tbody>
                                                    <thead>
                                                        <tr>
                                                            <th style="text-align: center; border: solid 1px #cccccc;" colspan="4" scope="col">ESTRUCTURA</th>
                                                        </tr>
                                                        <tr>
                                                            <th style="text-align: center; border: solid 1px #cccccc;" colspan="4">Dimensiones</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <th scope="col" style="border: solid 1px #cccccc;">Horizontal:</th>
                                                            <td style="text-align: center; border: solid 1px #cccccc;">'.$longitud_horizontal.' mts</td>
                                                            <th scope="col" style="border: solid 1px #cccccc;">Vertical:</th>
                                                            <td style="text-align: center; border: solid 1px #cccccc;">'.$longitud_vertical.' mts</td>
                                                        </tr>
                                                    </tbody>
                                                    <thead>
                                                        <tr>
                                                            <th style="text-align: center" colspan="4">Medidas de Cañer&iacutea Principal</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <th scope="col" style="border: solid 1px #cccccc;">Di&aacutemetro de Caño:</th>
                                                            <td style="text-align: center; border: solid 1px #cccccc;">'.$canio.'"</td>
                                                            <th scope="col" style="border: solid 1px #cccccc;">Acople Vitaulic:</th>
                                                            <td style="text-align: center; border: solid 1px #cccccc;">'.$acople_vit.'"</td>
                                                        </tr>
                                                    </tbody>
                                                    <thead>
                                                        <tr>
                                                            <th style="text-align: center; border: solid 1px #cccccc;" colspan="4">Medidas de V&aacutelvulas</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <th scope="col" style="border: solid 1px #cccccc;">Limpieza:</th>
                                                            <td style="text-align: center; border: solid 1px #cccccc;">'.$val_limpieza.'"</td>
                                                            <th scope="col" style="border: solid 1px #cccccc;">Esf&eacutericas:</th>
                                                            <td style="text-align: center; border: solid 1px #cccccc;">'.$val_esfe.'"</td>
                                                        </tr>
                                                    </tbody>
                                                    <thead>
                                                        <tr>
                                                            <th style="text-align: center; border: solid 1px #cccccc;" colspan="4">Salidas</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <th style="text-align: center; border: solid 1px #cccccc;" scope="col" colspan="2">Cantidad:</th>
                                                            <td style="text-align: center; border: solid 1px #cccccc;" colspan="2">'.$cant_salidas.'</td>
                                                        </tr>';
                                                        $qu=$mysqli->query("select * from cotizar_detalle where id_cotizar='$id_cotizar' order by id asc");
                                                        while($salidas=$qu->fetch_object()){
                                                        $qn=$mysqli->query("select mayor from catalogo where id='$salidas->niple'");
                                                        $niple=$qn->fetch_object();
                                                        $qv=$mysqli->query("select mayor from catalogo where id='$salidas->valvula'");
                                                        $valvula=$qv->fetch_object();
                                                        $mensaje1=$mensaje1.'
                                                        <tr>
                                                        <td scope="col" style="border: solid 1px #cccccc;">Medida Niple:</td>
                                                        <td style="text-align: center; border: solid 1px #cccccc;">'.$niple->mayor.'"</td>
                                                        <td scope="col" style="border: solid 1px #cccccc;">Madida V&aacutelvula:</td>
                                                        <td style="text-align: center; border: solid 1px #cccccc;">'.$valvula->mayor.'"</td>
                                                        </tr>';
                                                        }
                
$mensaje2='
              </tbody>
      </table>
  

</td>
</tr>
<tr>
<td bgcolor="#70bbd9" style="padding: 30px 30px 30px 30px;">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;" width="75%">
&reg; H2Obras, Argentina 2019<br/>

</td>
<td align="right" width="25%">
<table border="0" cellpadding="0" cellspacing="0">
  <tr>
      <td style="font-family: Arial, sans-serif; font-size: 12px; font-weight: bold;">
          <a href="#" style="color: #ffffff;">
              <img src="images/tw.gif" alt="Twitter" width="38" height="38" style="display: block;" border="0" />
          </a>
      </td>
      <td style="font-size: 0; line-height: 0;" width="20">&nbsp;</td>
      <td style="font-family: Arial, sans-serif; font-size: 12px; font-weight: bold;">
          <a href="#" style="color: #ffffff;">
              <img src="images/fb.gif" alt="Facebook" width="38" height="38" style="display: block;" border="0" />
          </a>
      </td>
  </tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>';

$mensaje=$mensaje1.$mensaje2;
//echo $mensaje;
// Para enviar un correo HTML, debe establecerse la cabecera Content-type
$headers .= "MIME-Version: 1.0\n"; 
$headers .= "Content-type: text/html; charset=iso-8859-1\n"; 
$headers .= 'From: Info H2Obras <info@h2obras.com>'."\n"; 
$headers .= "To: ".$nombre_usuario . " <" . $email_usuario . ">\n";
//$headers .= "Reply-To: " . $FromMail . "\n"; 
$headers .= "X-Priority: 1\n"; 
$headers .= "X-MSMail-Priority: High\n"; 
$headers .= "X-Mailer: Widgets.com Server"; 
//$cabeceras  = 'MIME-Version: 1.0' . "\r\n";
//$cabeceras .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

// Cabeceras adicionales
//$cabeceras .= 'To: '.$nombre_usuario.' <'.$email_usuario.'>'. "\r\n";
//$cabeceras .= 'From: Info H2Obras <info@h2obras.com>' . "\r\n";

// Enviarlo
mail($para, $titulo, $mensaje, $headers);

header("Location:../index.php?page=ven&cot=ok");
?>